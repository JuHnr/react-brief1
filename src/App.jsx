import { useState } from 'react'
import './App.css'
import Contact from './components/Contact.jsx' //importe le composant Contact

function App() {

  return (
    <>

      <div className='title'>
        <h1>Mes contacts</h1>
      </div>

      {/* ajoute 3 composants Contact avec des valeurs de props différents */}

      <Contact name="Chantal BERT" image="profile.jpg" isConnected={true} />
      <Contact name="Alain TERIEUR" image="profile2.jpg" isConnected={false} />
      <Contact name="Camille ONETTE" image="profile3.jpg" isConnected={true} />

    </>
  )
}

//exporte la fonction App (pour main.jsx)
export default App
