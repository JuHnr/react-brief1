import "./Contact.css"; //importe le fichier de style associé au composant

//créé et exporte la fonction/composant Contact (pour pouvoir être réutilisé)
//requiert des props
export default function Contact(props) {
    return (
        <> {/* Fragment : nécessaire car il faut qu'un seul parent pour un composant */}

            <div className="profile_card">  

                {/* vient chercher la source de l'image donnée lors de l'ajout du composant dans l'application */}
                <img className="profile_image" src={props.image} alt="" /> 

                {/* vient chercher la valeur de isConnected. Si true alors ajoute la classe .profile_status--online, sinon n'ajoute rien */}
                <span className={`profile_status ${props.isConnected ? "profile_status--online" : ""}`}></span>

            </div >

            {/* vient chercher la valeur de name lors de l'ajout du composant dans l'application */}
            <p className="profile_name">{props.name}</p>
            
        </>
    )

}

